<?php

/* @App/templates/toptoll.html.twig */
class __TwigTemplate_ed0f5244906bf55737220e6f25bfb6c7097159a503558e84d26d7a33c165091d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/toptoll.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/toptoll.html.twig"));

        // line 1
        echo "<div class=\"divlogo flex\"><img class=\"logo\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"logo\"></div>
<div class=\"container\">

    <div class=\"row destacados quest\">
        <div class=\"col-4 q1\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta1\"></p>
                    <p class=\"author\">By: <span id=\"name1\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q2\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta2\"></p>
                    <p class=\"author\">By: <span id=\"name2\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q3\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta3\"></p>
                    <p class=\"author\">By: <span id=\"name3\"></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row destacados quest\">
        <div class=\"col-4 q4\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta4\"></p>
                    <p class=\"author\">By: <span id=\"name4\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q5\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta5\"></p>
                    <p class=\"author\">By: <span id=\"name5\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q6\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta6\"></p>
                    <p class=\"author\">By: <span id=\"name6\"></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row destacados quest\">
        <div class=\"col-4 q7\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta7\"></p>
                    <p class=\"author\">By: <span id=\"name7\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q8\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta8\"></p>
                    <p class=\"author\">By: <span id=\"name8\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q9\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta9\"></p>
                    <p class=\"author\">By: <span id=\"name9\"></span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/encuestasdestacadas.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/redireclogin.js"), "html", null, true);
        echo "\"></script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@App/templates/toptoll.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 86,  113 => 85,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"divlogo flex\"><img class=\"logo\" src=\"{{ asset('img/logo.png') }}\" alt=\"logo\"></div>
<div class=\"container\">

    <div class=\"row destacados quest\">
        <div class=\"col-4 q1\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta1\"></p>
                    <p class=\"author\">By: <span id=\"name1\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q2\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta2\"></p>
                    <p class=\"author\">By: <span id=\"name2\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q3\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta3\"></p>
                    <p class=\"author\">By: <span id=\"name3\"></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row destacados quest\">
        <div class=\"col-4 q4\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta4\"></p>
                    <p class=\"author\">By: <span id=\"name4\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q5\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta5\"></p>
                    <p class=\"author\">By: <span id=\"name5\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q6\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta6\"></p>
                    <p class=\"author\">By: <span id=\"name6\"></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row destacados quest\">
        <div class=\"col-4 q7\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta7\"></p>
                    <p class=\"author\">By: <span id=\"name7\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q8\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta8\"></p>
                    <p class=\"author\">By: <span id=\"name8\"></span></p>
                </div>
            </div>
        </div>
        <div class=\"col-4 q9\">
            <div class=\"card clickresultados\" style=\"width: 100%;\">
                <div class=\"card-body\">
                    <p class=\"pregunta9\"></p>
                    <p class=\"author\">By: <span id=\"name9\"></span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src=\"{{ asset('js/encuestasdestacadas.js') }}\"></script>
<script src=\"{{ asset('js/redireclogin.js') }}\"></script>", "@App/templates/toptoll.html.twig", "/home/ausias/Escriptori/thepoll/src/AppBundle/Resources/views/templates/toptoll.html.twig");
    }
}
