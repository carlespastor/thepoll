<?php

/* @App/templates/login.html.twig */
class __TwigTemplate_3ea3b55a7deec2da246bf68a8fee744706a0c59e39f9558d78fbdf74ffd0964c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/login.html.twig"));

        // line 1
        echo "<div class=\"divlogo flex\"><img class=\"logo\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"logo\"></div>
<div class=\"loginsquare mx-auto\" style=\"width: 25rem;\">
    <div class=\"form-group\">
        <label for=\"user\">Nom d'usuari</label>
        <input type=\"text\" class=\"form-control\" id=\"user\" aria-describedby=\"emailHelp\" placeholder=\"Enter email\">
    </div>
    <div class=\"form-group\">
        <label for=\"pass\">Contrasenya</label>
        <input type=\"password\" class=\"form-control\" id=\"pass\" placeholder=\"Password\">
    </div>
    <button id=\"submit\" class=\"btn\">Submit</button>
    <p class=\"error\"></p>
    <p><span id=\"u\">User </span><span id=\"a\"> Admin </span><span style=\"color: red;\"> Borrar luego!</span></p>
</div>
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/login.js"), "html", null, true);
        echo "\"></script>
<script>
    \$(\"#u\").click(rellenauser);
    \$(\"#u, #a\").css(\"cursor\", \"pointer\");
    \$(\"#a\").click(rellenadmin);

    function rellenauser(){
        \$(\"#user\").val(\"carles\");
        \$(\"#pass\").val(\"carles\");
    }
    function rellenadmin(){
        \$(\"#user\").val(\"admin\");
        \$(\"#pass\").val(\"admin\");
    }
</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@App/templates/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 15,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"divlogo flex\"><img class=\"logo\" src=\"{{ asset('img/logo.png') }}\" alt=\"logo\"></div>
<div class=\"loginsquare mx-auto\" style=\"width: 25rem;\">
    <div class=\"form-group\">
        <label for=\"user\">Nom d'usuari</label>
        <input type=\"text\" class=\"form-control\" id=\"user\" aria-describedby=\"emailHelp\" placeholder=\"Enter email\">
    </div>
    <div class=\"form-group\">
        <label for=\"pass\">Contrasenya</label>
        <input type=\"password\" class=\"form-control\" id=\"pass\" placeholder=\"Password\">
    </div>
    <button id=\"submit\" class=\"btn\">Submit</button>
    <p class=\"error\"></p>
    <p><span id=\"u\">User </span><span id=\"a\"> Admin </span><span style=\"color: red;\"> Borrar luego!</span></p>
</div>
<script src=\"{{ asset('js/login.js') }}\"></script>
<script>
    \$(\"#u\").click(rellenauser);
    \$(\"#u, #a\").css(\"cursor\", \"pointer\");
    \$(\"#a\").click(rellenadmin);

    function rellenauser(){
        \$(\"#user\").val(\"carles\");
        \$(\"#pass\").val(\"carles\");
    }
    function rellenadmin(){
        \$(\"#user\").val(\"admin\");
        \$(\"#pass\").val(\"admin\");
    }
</script>", "@App/templates/login.html.twig", "/home/ausias/Escriptori/thepoll/src/AppBundle/Resources/views/templates/login.html.twig");
    }
}
