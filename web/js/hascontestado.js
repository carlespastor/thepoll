//Funcion que captura la funcion de los botones "contesta"
function hascontestado(idtoll, idbuttonclicked){
    //Si ya esta contestada el boton mostrara resultados, si no mostrara el formulario para contestarla.
    var userId = getidUser("idUser");
    parametros={
        idpregunta : idtoll,
        iduser : userId
    };

    console.log(parametros);

    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/comprovantepregunta.php',
        dataType : 'text',
        success: function (data){
            if (data=="SI") {
                $("#b" + idbuttonclicked).html("Ver Resultados");
                $("#b" + idbuttonclicked).click(function () {
                    $(".contentrespuesta" + idbuttonclicked).html(
                        '<div style="width:20%"><canvas id="myChart' + idtoll + '" width="50" height="50"></canvas></div>');
                    muestragraficos(idtoll, idbuttonclicked);
                });
            }
            else{
                if ($(".b" + idbuttonclicked).parent().hasClass("caducada")){
                    console.log("caducada -->" + idbuttonclicked);
                    $("#b" + idbuttonclicked).click(function () {
                        $(".contentrespuesta" + idbuttonclicked).html('<p>No hi ha cap resultat</p>');
                    });
                }
                else {
                    $("#b" + idbuttonclicked).click(function () {
                        $(".contentrespuesta" + idbuttonclicked).html('<div id="respon' + idtoll + '"></div>');
                        contestapregunta(idtoll, idbuttonclicked);
                    });
                }
            };
        },
        error: function (data){
            console.log("Error en comprovacion de preguntas");
        }
    })
}

function muestragraficos(idtoll, idbuttonclicked){

    parametros={
        idpregunta : idtoll
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/respuestas.php',
        dataType : 'JSON',
        success: function (data){

            console.log(data[0].si);
            console.log(data[0].no);

            var ctx = document.getElementById("myChart" + idtoll);
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ["Si", "No"],
                    datasets: [{
                        label: '# of Votes',
                        data: [data[0].si, data[0].no],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },

            });

        },
        error: function (data){
            console.log("Error en comprovacion de respuestas");
        }
    })
    muestrarespuestauser(idtoll, idbuttonclicked);
}

function contestapregunta(idtoll, idbuttonclicked){
    $("#respon" + idtoll).html('<div id="buttonsrespuesta"><button id="si' + idtoll + '">Si</button>' +
        '<button id="no' + idtoll + '">No</button></div>');

    $("#si"+idtoll).click(function () {
        console.log("has respondido que si");
        $("#b" + idbuttonclicked).html("Ver Resultados");

        $("#buttonsrespuesta").hide();
        insertrespuesta(1,idtoll);
        hascontestado(idtoll,idbuttonclicked);

        setTimeout(function () {
            $("#b" + idbuttonclicked).trigger('click');
        }, 200);

    })

    $("#no"+idtoll).click(function () {
        console.log("has respondido que no");
        $("#b" + idbuttonclicked).html("Ver Resultados");


        $("#buttonsrespuesta").hide();

        insertrespuesta(0,idtoll);
        hascontestado(idtoll,idbuttonclicked);

        setTimeout(function () {
            $("#b" + idbuttonclicked).trigger('click');
        }, 100);
    })
}

function insertrespuesta(respuesta, idtoll){

    parametros={
        contentrespuesta : respuesta,
        idpregunta: idtoll
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/insert.php',
        dataType : 'text',
        success: function (data){

        },
        error: function (data){
            console.log("Error en el insert");
        }
    })
}

function muestrarespuestauser(idtoll, idbuttonclicked) {
    parametros={
        idpregunta: idtoll
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/muestrarespuestas.php',
        dataType : 'text',
        success: function (data){
            console.log(data);
            console.log("#respuesta" + idbuttonclicked);
            if(data == 1){
                $("#respuesta" + idbuttonclicked).html("Tu respuesta fue si");
            }else{
                $("#respuesta" + idbuttonclicked).html("Tu respuesta fue no");
            }
        },
        error: function (data){
            console.log("Error en las respuesta");
        }
    })
}