//Funcion JS que recoge todas las preguntas y las muestra.
function dameEncuestas(){
    $.ajax({
        type:'POST',
        url: 'php/encuestas.php',
        dataType : 'JSON',
        success: function (datos){

            var limit = (Object.keys(datos).length);
            muestraEncuestas(limit, datos);

            var username = getidUser("uname");
            $("#nameadmin").html(username);
            $("#nameadmin").css("text-transform", "capitalize");
            $("#logout").click(logout);
        },
        error: function (datos){
            console.log("Error en encuestas usuario");
        }
    })

    //Funcion que muestra todas las encuestas y comprueba si son destacadas
    function muestraEncuestas(limit, datos){
        $('#myTab').css('display', "none");

        $.ajax({
            type:'POST',
            url: 'php/destacadas.php',
            dataType : 'JSON',
            success: function (data){


                var limitDestacadas = (Object.keys(data).length);
                for (var i=0;i<limit;i++) {
                    var comprovacio=0;
                    for (var j = 0; j < limitDestacadas; j++) {
                        if (data[j].pregunta == datos[i].pregunta) {
                            comprovacio = 1;
                            console.log(data[j].pregunta + "==" + datos[i].pregunta);
                        }
                    }


                    var start = new Date(datos[i].data_final),
                        end   = new Date(),
                        diff  = new Date(end - start),
                        days  = diff/1000/60/60/24;

                    var diferenciadies = Math.round(-days);
                    console.log("Dias restantes "+ (diferenciadies+1));

                    if((diferenciadies + 1)<0){
                        var fecha = '<span class="expiredate finished">' + datos[i].data_final + '</span>';
                        $(".cadpoll").append('<div class="card m3 caducada"><div class="card-body card-body-pregunta b' + (i + 1) + '"><span class="question">' + (datos[i].pregunta) + '</span><span id="b' + (i + 1) + '" class="btn-answer">Ver Resultados</span></div><div class="contentrespuesta' + (i + 1) + ' container"></div><p id="respuesta' + (i + 1) + '"></p><div class="card-footer text-muted text-center">Valido hasta: ' + fecha + ' </div></div>');
                    }
                    else {
                        if (diferenciadies <= 3)
                            var fecha = '<span class="expiredate finished">' + datos[i].data_final + '</span>';

                        else var fecha = '<span class="expiredate">' + datos[i].data_final + '</span>';


                        if (comprovacio == 1) {
                            $(".contentajax").append('<div class="card m3 destacada"><div class="card-body card-body-pregunta b' + (i + 1) + '"><span class="question">' + (datos[i].pregunta) + '</span><span id="b' + (i + 1) + '" class="btn-answer">Contestar</span></div><div class="contentrespuesta' + (i + 1) + ' container"></div><p id="respuesta' + (i + 1) + '"></p><div class="card-footer text-muted text-center">Valido hasta: ' + fecha + ' </div></div>');
                            console.log("no --> Es igual");
                        } else {
                            $(".contentajax").append('<div class="card m3"><div class="card-body card-body-pregunta b' + (i + 1) + '"><span class="question">' + (datos[i].pregunta) + '</span><span id="b' + (i + 1) + '" class="btn-answer">Contestar</span></div><div class="contentrespuesta' + (i + 1) + ' container"></div><p id="respuesta' + (i + 1) + '"></p><div class="card-footer text-muted text-center">Valido hasta: ' + fecha + '</div></div>');
                            console.log("no --> No es igual");
                        }
                    }


                    hascontestado(datos[i].id, i+1);

                }
            },
            error: function (data){
                console.log("Error en identificacion de destacadas");
            }
        })
    }
}

//Funcion de js que borra la cookie
function logout() {
    document.cookie = 'rol=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'uname=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'comp=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'idUser=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    //location.reload();
}

function getidUser(cname){

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";

}



